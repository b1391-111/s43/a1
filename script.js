// Event Listener
const firstName = document.querySelector('#firstN');
const lastName = document.querySelector('#lastN');
const fullName = document.querySelector('#fullN');

function getFullName() {
    let inputFN = firstName.value;
    let inputLN = lastName.value;
    fullName.innerHTML = `${inputFN} ${inputLN}`;
}
    
firstName.addEventListener("keyup", getFullName);

lastName.addEventListener("keyup", getFullName);